/*! @file ili9341.c
 *
 * @brief  Quadruple half-H driver
 *
 * This driver provide functions to configure and control a dual DC motor driver
 * using the L293D.
 *
 * @author Albano Peñalva
 *
 * @note Hardware connections:
 *
 * |   	L293D		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	5V		 	|	5V			|
 * | 	GND		 	| 	GND			|
 * | 	1,2EN	 	| 	TFIL2		|
 * | 	1A		 	| 	TCOL2		|
 * | 	2A		 	| 	TCOL0		|
 * | 	3,4EN	 	| 	TFIL1		|
 * | 	3A		 	| 	TCOL1		|
 * | 	4A		 	| 	TFIL0		|
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 05/10/2022 | Document creation		                         |
 *
 */

#include "l293.h"
#include "gpio.h"
#include "pwm_sct.h"

/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/

#define MAX_F_SPEED 	100		/*!< Max foward speed  */
#define MAX_B_SPEED 	-100	/*!< Max backward speed */
#define PWM_FREQ 		10		/*!< PWM frequency (Hz) */
#define N_MOTORS		2		/*!< Number of motors */

pwm_out_t pwm_out[] = {CTOUT0, CTOUT1};

/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

uint8_t L293Init(void){
	PWMInit(pwm_out, N_MOTORS, PWM_FREQ);
	GPIOInit(GPIO_T_COL0, GPIO_OUTPUT);
	GPIOInit(GPIO_T_COL2, GPIO_OUTPUT);
	GPIOInit(GPIO_T_FIL3, GPIO_OUTPUT);
	GPIOInit(GPIO_T_FIL0, GPIO_OUTPUT);
	PWMOn();

	return 1;
}

uint8_t L293SetSpeed(l293_motor_t motor, int8_t speed){
	uint8_t err = 0;

	switch(motor){
	case MOTOR_1:
		if(speed == 0){
			PWMSetDutyCycle(pwm_out[0], speed);
			GPIOOff(GPIO_T_COL0);
			GPIOOff(GPIO_T_COL2);
		}
		if(speed > 0){
			if (speed > MAX_F_SPEED) speed = MAX_F_SPEED;
			PWMSetDutyCycle(pwm_out[0], speed);
			GPIOOn(GPIO_T_COL2);
			GPIOOff(GPIO_T_COL0);
		}
		if(speed < 0){
			if (speed < MAX_B_SPEED) speed = MAX_B_SPEED;
			PWMSetDutyCycle(pwm_out[0], -speed);
			GPIOOn(GPIO_T_COL0);
			GPIOOff(GPIO_T_COL2);
		}
		break;
	case MOTOR_2:
		if(speed == 0){
			PWMSetDutyCycle(pwm_out[1], speed);
			GPIOOff(GPIO_T_FIL3);
			GPIOOff(GPIO_T_FIL0);
		}
		if(speed > 0){
			if (speed > MAX_F_SPEED) speed = MAX_F_SPEED;
			PWMSetDutyCycle(pwm_out[1], speed);
			GPIOOn(GPIO_T_FIL3);
			GPIOOff(GPIO_T_FIL0);
		}
		if(speed < 0){
			if (speed < MAX_B_SPEED) speed = MAX_B_SPEED;
			PWMSetDutyCycle(pwm_out[1], -speed);
			GPIOOn(GPIO_T_FIL0);
			GPIOOff(GPIO_T_FIL3);
		}
		break;
	default:
		err = 1;
		break;
	}

	return err;
}

uint8_t L293DeInit(void){
	PWMOff();

	return 1;
}
