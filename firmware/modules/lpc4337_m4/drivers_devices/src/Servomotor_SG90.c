/*! @mainpage Servomotor_SG90
 *
 * \section genDesc General Description
 *
 * Driver para servomotor SG90, inicializa el PWM necesario para el uso del servo
 * y convierte grados en tiempo en alto, de esta manera variar el ciclo del trabajo del PWM
 * para poder así mover los brazos del servomotor.
 *
 * \section hardConn Hardware Connection
 *
 * | SERVOMOTORSG90	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 		PWM	 	| 	T_FIL2		|
 * | 		5V 		| 	5V			|
 * | 		GND	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 *
 * @author Acosta Azul
 *
 */
/*==================[inclusions]=============================================*/

#include "gpio.h"
#include "chip.h"
#include "SERVOMOTOR_SG90.h"
#include "pwm_sct.h"
/*==================[macros and definitions]=================================*/

/*Datos de la datasheet del servomotor SG90*/

#define Periodo_Total 20.0 /*Periodo Total*/
#define Frecuencia 50 /*Frecuencia 50Hz*/

/*==================[internal data declaration]==============================*/
/*Variables necesarias*/

uint8_t grados;
float Tiempo_Alto;
uint8_t Ciclo_Trabajo;
uint8_t Nro_Output=1;
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

bool ServoInit(){
	pwm_out_t pon[]= {CTOUT0};
	PWMInit(pon, Nro_Output, Frecuencia);
	PWMOn();

return true;
}

bool ServoMover(int16_t grados){

	grados=grados+90;
	Tiempo_Alto=(float)(grados/180.0)+1;
	Ciclo_Trabajo=(float)(Tiempo_Alto/Periodo_Total)*100;
	PWMSetDutyCycle(CTOUT0, Ciclo_Trabajo);

return true;
}




/*==================[end of file]============================================*/


