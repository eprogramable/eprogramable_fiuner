/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup TCS230 TCS230
 ** @{
 * @brief  Color Light-to-Frequency Converter
 *
 * This driver provide functions to configure and control a TCS230
 * color sensor.
 *
 * @author Albano Peñalva
 *
 * @note Hardware connections:
 *
 * |   	TCS230		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	|	5V			|
 * | 	GND		 	| 	GND			|
 * | 	S0		 	| 	GPIOx		|
 * | 	S1		 	| 	GPIOx		|
 * | 	S2		 	| 	GPIOx		|
 * | 	S3		 	| 	GPIOx		|
 * | 	OUT		 	| 	GPIOx		|
 * | 	LED		 	| 	-			|
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/10/2022 | Document creation		                         |
 *
 */

#include "tcs230.h"

#include "gpio.h"
#include "delay.h"

/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/

#define MAX_FREQ_RED 	15000	/*!< Max frequency for color red in Hz */
#define MAX_FREQ_BLUE	15000	/*!< Max frequency for color blue in Hz */
#define MAX_FREQ_GREEN	15000	/*!< Max frequency for color green in Hz */
#define MIN_FREQ		1500	/*!< Min frequency (color black) */
#define FULL_SCALE		255		/*!< Max value for any color  */
#define MICROSEC		1000000	/*!< Max value for any color  */

/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

gpio_t gpio_out, gpio_s0, gpio_s1, gpio_s2, gpio_s3;

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

uint8_t TCS230Init(gpio_t out, gpio_t s0, gpio_t s1, gpio_t s2, gpio_t s3){

	gpio_s0 = s0;
	gpio_s1 = s1;
	GPIOInit(s0, GPIO_OUTPUT);
	GPIOInit(s1, GPIO_OUTPUT);
	/* S0=H & S1=L -> Frequency Scaling 20% (~120kHz @ full scale) */
	GPIOOn(s0);
	GPIOOff(s1);

	gpio_s2 = s2;
	gpio_s3 = s3;
	GPIOInit(s2, GPIO_OUTPUT);
	GPIOInit(s3, GPIO_OUTPUT);

	gpio_out = out;
	GPIOInit(out, GPIO_INPUT);

	return 1;
}

uint32_t TCS230ReadRed(void) {
	uint8_t i;
	const uint8_t N = 10;
	uint16_t pulse_width;
	uint32_t freq, red;
	/* S2=L & S3=L -> Red color photodiode */
	GPIOOff(gpio_s2);
	GPIOOff(gpio_s3);
	pulse_width = 0;
	DelayMs(200);
	/* Read low pulse duration */
	for (i = 0; i < N; i++) {
		while (GPIORead(gpio_out)) {

		}
		while (!GPIORead(gpio_out)) {
			DelayUs(1);
			pulse_width++;
		}
	}
	pulse_width = pulse_width / N;
	freq = MICROSEC / (4 * pulse_width);

	red = (freq - MIN_FREQ) * FULL_SCALE / MAX_FREQ_RED;
	if(red > FULL_SCALE){
		red = FULL_SCALE;
	}
	return (uint8_t)red;
}

uint32_t TCS230ReadBlue(void){
	uint8_t i;
	const uint8_t N = 10;
	uint16_t pulse_width;
	uint32_t freq, blue;
	/* S2=L & S3=H -> Blue color photodiode */
	GPIOOff(gpio_s2);
	GPIOOn(gpio_s3);
	pulse_width = 0;
	DelayMs(200);
	/* Read low pulse duration */
	for (i = 0; i < N; i++) {
		while (GPIORead(gpio_out)) {

		}
		while (!GPIORead(gpio_out)) {
			DelayUs(1);
			pulse_width++;
		}
	}
	pulse_width = pulse_width / N;
	freq = MICROSEC / (4 * pulse_width);

	blue = (freq - MIN_FREQ) * FULL_SCALE / MAX_FREQ_BLUE;
	if(blue > FULL_SCALE){
		blue = FULL_SCALE;
	}
	return (uint8_t)blue;
}

uint32_t TCS230ReadGreen(void){
	uint8_t i;
	const uint8_t N = 10;
	uint16_t pulse_width;
	uint32_t freq, green;
	/* S2=H & S3=H -> Blue color photodiode */
	GPIOOn(gpio_s2);
	GPIOOn(gpio_s3);
	pulse_width = 0;
	DelayMs(200);
	/* Read low pulse duration */
	for (i = 0; i < N; i++) {
		while (GPIORead(gpio_out)) {

		}
		while (!GPIORead(gpio_out)) {
			DelayUs(1);
			pulse_width++;
		}
	}
	pulse_width = pulse_width / N;
	freq = MICROSEC / (4 * pulse_width);

	green = (freq - MIN_FREQ) * FULL_SCALE / MAX_FREQ_GREEN;
	if(green > FULL_SCALE){
		green = FULL_SCALE;
	}
	return (uint8_t)green;
}

uint8_t TCS230DeInit(void){

	/* Power down */
	GPIOOff(gpio_s0);
	GPIOOff(gpio_s1);
	return 1;
}
