/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup TCS230 TCS230
 ** @{ 
 * @brief  Color Light-to-Frequency Converter
 *
 * This driver provide functions to configure and control a TCS230
 * color sensor.
 *
 * @author Albano Peñalva
 *
 * @note Hardware connections:
 *
 * |   	TCS230		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	|	5V			|
 * | 	GND		 	| 	GND			|
 * | 	S0		 	| 	GPIOx		|
 * | 	S1		 	| 	GPIOx		|
 * | 	S2		 	| 	GPIOx		|
 * | 	S3		 	| 	GPIOx		|
 * | 	OUT		 	| 	GPIOx		|
 * | 	LED		 	| 	-			|
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 14/10/2022 | Document creation		                         |
 *
 */

#ifndef TCS230_H_
#define TCS230_H_

#include <stdint.h>
#include <gpio.h>

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/



/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief  		Initializes TCS230 driver
 * @param[in]  	out:	GPIO to read output signal
 * @param[in]  	s0:		GPIO to control frequency scaling
 * @param[in]  	s1:		GPIO to control frequency scaling
 * @param[in]  	s2:		GPIO to control photodiode filter color
 * @param[in]  	s3:		GPIO to control photodiode filter color
 * @retval 		1 when success, 0 when fails
 */
uint8_t TCS230Init(gpio_t out, gpio_t s0, gpio_t s1, gpio_t s2, gpio_t s3);

/**
 * @brief  		Reads color red
 * @param[in]  	None
 * @retval 		level of red light: 0 to 255
 */
uint32_t TCS230ReadRed(void);

/**
 * @brief  		Reads color blue
 * @param[in]  	None
 * @retval 		level of blue light: 0 to 255
 */
uint32_t TCS230ReadBlue(void);

/**
 * @brief  		Reads color green
 * @param[in]  	None
 * @retval 		level of green light: 0 to 255
 */
uint32_t TCS230ReadGreen(void);

/**
 * @brief  	De-initializes TCS230 Driver
 * @param	None
 * @retval 	1 when success, 0 when fails
 */
uint8_t TCS230DeInit(void);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */

#endif /* TCS230_H_ */
