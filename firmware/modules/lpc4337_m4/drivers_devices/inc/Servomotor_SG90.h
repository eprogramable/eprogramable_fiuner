/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup SERVOMOTOR_SG90
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Azul Acosta
 *
 * Driver para servomotor SG90, inicializa el PWM necesario para el uso del servo
 * y convierte grados en tiempo en alto, de esta manera variar el ciclo del trabajo del PWM
 * para poder así mover los brazos del servomotor.
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 07/10/2021 | Document creation		                         |
 * | 04/11/2021	| Documento finalizado						     |
 */

#ifndef SERVOMOTOR_SG90_H_
#define SERVOMOTOR_SG90_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/**
 * @brief Inicializa PWM del servomotor SG90
 * @param[in] Ninguno
 * @return TRUE
 */
bool ServoInit();

/**
 * @brief Convierte grados en tiempo en alto del ciclo de trabajo del PWM
 * @param[in] Entero con signo de 16 bits grados
 * @return TRUE
 */
bool ServoMover(int16_t grados);


#endif /* SERVOMOTOR_SG90_H_ */




